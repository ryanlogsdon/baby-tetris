from ssd1306 import SSD1306_I2C
from machine import Pin, I2C
import utime
import random

button_left = Pin(26, Pin.IN, Pin.PULL_UP)
button_right = Pin(27, Pin.IN, Pin.PULL_UP)

i2c = I2C(1, scl=Pin(7), sda=Pin(6))
oled = SSD1306_I2C(128, 64, i2c)

LOGICAL_DISPLAY_WIDTH = oled.height         # use the physical height as the logical width
LOGICAL_DISPLAY_HEIGHT = oled.width         # use the physical width as the logical height

SCALE = 4       # scale factor for the shapes (1 = 1 pixel, 2 = 2x2 pixels, 3 = 3x3 pixels...)

shapes = {
    'I': [[[1, 1, 1, 1]]],  # straight line shape
    'O': [[[1, 1],
           [1, 1]]],        # square shape
    'L': [[[1, 0, 0],
           [1, 1, 1]]],     # L-shape
    'Z': [[[1, 1, 0], 
           [0, 1, 1]]],     # Z-shape
    'S': [[[0, 1, 1], 
           [1, 1, 0]]],     # S-shape
}

# the playfield will track where the shapes have landed
PLAYFIELD_WIDTH = LOGICAL_DISPLAY_WIDTH // SCALE
PLAYFIELD_HEIGHT = LOGICAL_DISPLAY_HEIGHT // SCALE
playfield = [[0 for x in range(PLAYFIELD_WIDTH)] for y in range(PLAYFIELD_HEIGHT)]


class Shape:
    def __init__(self, shape):
        self.shape = shape
        self.shape_width = len(shape[0]) * SCALE
        self.x = (LOGICAL_DISPLAY_WIDTH - self.shape_width) // 2     # adjust starting x-position
        self.y = 0

    def draw(self, oled):
        for i, row in enumerate(self.shape):
            for j, val in enumerate(row):
                if val:                             # only draw '1's in the shape array
                    for dx in range(SCALE):
                        for dy in range(SCALE):
                            self.draw_pixel_portrait(oled, self.x + j * SCALE + dx, self.y + i * SCALE + dy, 1)

    # convert landscape to portrait mode
    def draw_pixel_portrait(self, oled, x, y, color):
        physical_x = y
        physical_y = LOGICAL_DISPLAY_WIDTH - 1 - x
        oled.pixel(physical_x, physical_y, color)

def check_buttons(shape):
    move_amount = SCALE
    
    # if the left-button was pressed and the shape isn't at the left edge of the screen
    if not button_left.value() and shape.x - move_amount >= 0:
        shape.x -= move_amount
        utime.sleep_ms(100)                 # debounce delay
        
    # if the right-button was pressed and the shape isn't already at the right edge of the screen
    if not button_right.value() and (shape.x + shape.shape_width + move_amount <= LOGICAL_DISPLAY_WIDTH):
        shape.x += move_amount
        utime.sleep_ms(100)                 # debounce delay

def add_current_shape_to_playfield(playfield, shape):
    for i, row in enumerate(shape.shape):
        for j, val in enumerate(row):
            if val:
                playfield_x = (shape.x // SCALE) + j
                playfield_y = (shape.y // SCALE) + i
                if 0 <= playfield_x < PLAYFIELD_WIDTH and 0 <= playfield_y < PLAYFIELD_HEIGHT:
                    playfield[playfield_y][playfield_x] = 1

def draw_playfield(oled, playfield):
    for y, row in enumerate(playfield):
        for x, val in enumerate(row):
            if val:
                # calculate the physical position based on the portrait orientation
                physical_x = y * SCALE
                physical_y = LOGICAL_DISPLAY_WIDTH - (x * SCALE) - SCALE  # Adjust Y position for portrait orientation
                
                # draw each pixel of the filled cell
                for dx in range(SCALE):
                    for dy in range(SCALE):
                        oled.pixel(physical_x + dx, physical_y + dy, 1)

def check_collision(playfield, shape):
    for i, row in enumerate(shape.shape):
        for j, val in enumerate(row):
            if val:
                playfield_x = (shape.x // SCALE) + j
                playfield_y = (shape.y // SCALE) + i
                if playfield_y >= PLAYFIELD_HEIGHT or playfield[playfield_y][playfield_x] == 1:
                    return True
    return False

def clear_full_rows(playfield):
    threshold = 0.7                     # we'll clear a row if x% or more of it is filled

    rows_to_clear = []
    for y, row in enumerate(playfield):
        filled_cells = sum(cell == 1 for cell in row)
        if filled_cells >= PLAYFIELD_WIDTH * threshold:  # check if the row is filled more than our threshold
            rows_to_clear.append(y)

    # clear the rows and move everything above them down
    for row_index in reversed(rows_to_clear):
        del playfield[row_index]                                    # remove the row!            
        playfield.insert(0, [0 for _ in range(PLAYFIELD_WIDTH)])    # add an empty row at the top


def game_loop():
    global playfield

    current_shape = Shape(random.choice(list(shapes.values()))[0])
    while True:
        oled.fill(0)                                    # clear the screen
        check_buttons(current_shape)                    # check if the user pressed any buttons
        
        if check_collision(playfield, current_shape):
            current_shape.y -= SCALE                                        # move the shape back up since it collided
            add_current_shape_to_playfield(playfield, current_shape)        # store this shape's final position in the playfield
            clear_full_rows(playfield)                                      # if there are full rows, clear them
            current_shape = Shape(random.choice(list(shapes.values()))[0])  # make a new shape to drop
        else:
            current_shape.y += SCALE // 2               # no collision, so move the shape down

        draw_playfield(oled, playfield)                 # draw the playfield of all previously-played shapes
        current_shape.draw(oled)                        # draw the current shape

        oled.show()                                     # update the display with all drawn content

        utime.sleep(0.05)                               # small delay to control the speed of the game loop

game_loop()
