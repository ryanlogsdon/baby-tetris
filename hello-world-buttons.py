from machine import Pin, I2C
import utime

button_left = Pin(26, Pin.IN, Pin.PULL_UP)
button_right = Pin(27, Pin.IN, Pin.PULL_UP)


def check_buttons():
    if not button_left.value():
        print("Left button pressed")
        utime.sleep_ms(100)                 # debounce delay
        
    if not button_right.value():
        print("right button pressed")
        utime.sleep_ms(100)                 # debounce delay


while True:
    check_buttons()                         # check if the user pressed any buttons
    utime.sleep(0.05)                       # small delay to control the speed of the game loop
