from ws2812 import WS2812
import utime
import machine

# board setup
power = machine.Pin(11, machine.Pin.OUT)
power.value(1)
led = WS2812(pin_num=12, led_count=1)

# define the colors
BLACK = (0, 0, 0)
RED = (255, 0, 0)
YELLOW = (255, 150, 0)
GREEN = (0, 255, 0)
CYAN = (0, 255, 255)
BLUE = (0, 0, 255)
PURPLE = (180, 0, 255)
WHITE = (255, 255, 255)
RAINBOW = (BLACK, RED, YELLOW, GREEN, CYAN, BLUE, PURPLE, WHITE)

# brightness levels to cycle through (0.0 to 1.0)
BRIGHTNESS_LEVELS = [0.01, 0.1, 0.2, 0.3] # , 0.5, 1.0]

while True:
    for brightness in BRIGHTNESS_LEVELS:
        print(f'brightness level: {brightness}')
        led.set_brightness(brightness)
        for color in RAINBOW:
            led.pixels_fill(color)
            led.pixels_show()
            utime.sleep(0.5)
